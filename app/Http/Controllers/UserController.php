<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;

class UserController extends Controller
{

    // Crear usuario

    public function createUser(Request $request) {

        $userName = $request -> input('userName');
        $age = $request -> input('age');
        $password = $request -> input('password');

        // Hasheamos el password
        $password = Hash::make($password);

        try {
            return User::create([
                'userName' => $userName,
                'age' => $age,
                'password' => $password
            ]);
        } catch (QueryException $error) {
            $eCode = $error -> errorInfo[1];

            if($eCode == 1062) {
                return response() -> json([
                    'error' => "Usuario ya registrado"
                ]);
            }
        }
    }


    // Buscar un usuario por el id

    public function searchUser(Request $request, $id) {

        return User::find($id);
    }
    
    // Buscar todos los usuarios

    public function allUser() {

        try {
            return User::all();
        } catch (QueryException $error) {
            return $error;
        }
    }

    // Actualizar o Modificar atributos del usuario

    public function modifyUser(Request $request, $id) {

        $userName = $request -> input('userName');
        $age = $request -> input('age');
        $password = $request -> input('password');

        // Hasheamos el password 
        $password = Hash::make($password);

        try {
            return User::find($id) -> update([
                'userName' => $userName,
                'age' => $age,
                'password' => $password
            ]);

        }catch (QueryException $error) {
            return $error;
        }
    }

    // Borramos usuarios

    public function deleteUser(Request $request, $id) {

        // $removeUser = User::find($id);

        try {
            return User::find($id) -> destroy($id);
        } catch (QueryException $error) {
            return $error;
        }
    }
}
