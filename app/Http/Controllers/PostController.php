<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;


class PostController extends Controller
{
    // Crear post

    public function createPost(Request $request, $user_id) {

        $title = $request -> input('title');
        $userId = $request -> input('user_id');

        if($userId != $user_id){
            return response()->json([
                'error'=> 'No estás autorizado a crear post'
            ]);
        }

        try {
            return Post::create([
                'title' => $title,
                'user_id' => $user_id
            ]);
        } catch (QueryException $error) {
            return $error;
        }
    }

    // Buscar post por id

    public function searchPostById($id) {

        try {
            return Post::select()
            -> where('posts.user_id', '=', $id)
            -> get();
        } catch (QueryException $error) {
            return $error;
        }
    }

    // Buscar todos los post

    public function searchAllPost() {

        try {
            return Post::all();
        } catch (QueryException $error) {
            return $error;
        }
    }

    // Modificar un post

    public function modifyPost(Request $request, $post_id,  $userId) {

        $checkPost = Post::find($post_id);
        //Comprobamos que el post existe
        if(!$checkPost){
            return response()->json([
                'error'=> 'El Post no existe'
            ]);
        }
        //Comprobamos que el user es el propietario del post
        if($checkPost['user_id'] != $userId){
            return response()->json([
                'error'=> 'No estas autorizado a editar este Post'
            ]);
        }

        try {
            return $checkPost -> update(['title' => $request -> title]);
        } catch (QueryException $error) {
            return $error;
        }
    }

    // Borrar un post

    public function deletePost($post_id, $userId) {

        $post = Post::find($post_id);

        if($post['user_id'] != $userId){
            return response()->json([
                'error'=> 'No estas autorizado a borrar este Post'
            ]);
        }


        if(!$post){
            return response()->json([
                'error'=> 'El post no existe'
            ]);
        }
        
        try {
            return $post::destroy([
                'id' => $post_id
            ]);
        } catch (QueryException $error) {
            return $error;
        }
    }
}
