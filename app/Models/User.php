<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = ['userName', 'password', 'age'];

    public function userPost() {
        return $this -> hasMany('App\Models\Post', 'idpost');
    }
}
