<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;



// Rutas controladoras del user

Route::post('/user', [UserController::class, 'createUser']);
Route::get('/user/{userName}', [UserController::class, 'searchUser']);
Route::get('/user', [UserController::class, 'allUser']);
Route::put('/user/{id}', [UserController::class, 'modifyUser']);
Route::delete('user/{id}', [UserController::class, 'deleteUser']);

//Rutas controladoras de post

Route::post('/post/{id}', [PostController::class, 'createPost']);
Route::get('/post/{id}', [PostController::class, 'searchPostById']);
Route::get('/post', [PostController::class, 'searchAllPost']);
Route::put('/post/{id}/{userId}', [PostController::class, 'modifyPost']);
Route::delete('/post/{id}/{userId}', [PostController::class, 'deletePost']);

