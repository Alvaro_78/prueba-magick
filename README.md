<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Sobre el Proyecto
He utilizado para instalarlo el comando `curl -s https://laravel.build/example-app | bash` que crea toda la estructura de carpetas, pero no he utilizado ese docker-compose, ya que anteriormente me dió problemas y decidí crear mi propio contenedor de docker con mysql.
He creado los dos modelos User y Post con su CRUD correspondiente y con relación one to many.

## Arrancar el Proyecto
Clonar el repositorio, entrar al directorio y ejecutar en terminal `composer install`, una vez instaladas las dependencias ejecutar en terminal `sh docker-run.sh`, esto levantara un contenedor de docker con el servicio de mysql, después ejecutar en terminal `php artisan serve` para levantar el servidor de artisan en el puerto 8000.

<img src=./diagram.png>

He utilizado workbench para crear el diagrama y Postman para comprobar los endpoints.
El usuario es root, el password 1234 y el nombre de la db es prueba-magic.
He creado una colección con los endpoints que he utilizado en Postman, dividida en dos carpetas, User y Post
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/9722e0e3021b2c598401)
